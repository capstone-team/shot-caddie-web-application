// components/Welcome.js
import React from 'react';
import './Welcome.css';
import './Navbar.css'
import { useState } from 'react';
import SignUpModal from './LoginAndSignUp/SignUpModal';

const Welcome = () => {
    const [isSignUpModalOpen, setIsSignUpModalOpen] = useState(false);
    const divStyle = {
        marginTop: '300px',  // Increase this value to move the text down
        
    };
    const h1Style = {
        fontSize: '48px',
        fontFamily: 'Roboto, sans-serif',
        color: 'black',
    };

    const pStyle = {
        fontSize: '22px',
        fontFamily: 'Roboto, sans-serif',
        color: 'black',
    };

    return (
        <div className='welcome_page' style={divStyle}>
            <div className='navbar-buttons'>
            <button className="btn-signup" onClick={() => setIsSignUpModalOpen(true)}>Sign Up</button>
                <SignUpModal
                    isOpen={isSignUpModalOpen}
                    onRequestClose={() => setIsSignUpModalOpen(false)}
                />
            </div>
            <h1 style={h1Style}>Welcome to Shot Caddie</h1>
            <p style={pStyle}>Use GPS to track your shots, view statistics, and connect with others</p>
        </div>
    );
}

export default Welcome;