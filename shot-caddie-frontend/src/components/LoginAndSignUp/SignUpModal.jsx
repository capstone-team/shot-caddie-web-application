// import necessary modules
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Modal from 'react-modal';
import './SignUpModal.css';
import { auth, googleProvider, signInWithPopup } from '../../firebase';
import { useAuth } from '../AuthContext';

// Set the root element for the modal
Modal.setAppElement('#root');

// SignUpModal component
// This component is a modal that allows the user to sign up using email or Google
const SignUpModal = ({ isOpen, onRequestClose }) => {

    // Set the initial state of the user email, password, and confirmPassword
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [statusMessage, setStatusMessage] = useState('');

    // Get the navigate function from the useNavigate hook
    const navigate = useNavigate();

    // Get the setIsLoggedIn function from the useAuth hook
    const { setIsLoggedIn } = useAuth();

    // Handler for signing up with email
    const handleSignup = async () => {
        // Check if the password and confirmPassword match
        if (password !== confirmPassword) {
            console.error('Passwords do not match');
            setStatusMessage('Passwords do not match, try again');
            return;
        }
        try {
            const response = await fetch('http://localhost:8000/users/signup', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ email, password })
            });

            if (response.ok) {
                const data = await response.json();
                console.log('Sign up successful:', data);
                localStorage.setItem('access_token', data.access_token);
                localStorage.setItem('email', email);
                setIsLoggedIn(true);
                setStatusMessage('Sign up successful');
                onRequestClose();
                navigate('/dashboard');
            } else {
                const errorData = await response.json();
                console.error('SignUp failed:', errorData);
            }
        } catch (error) {
            console.error('SignUp failed:', error);
        }
    };

    // Handler for signing up with Google
    const handleGoogleSignUp = async () => {
        try {
            const result = await signInWithPopup(auth, googleProvider);
            const user = result.user;
            console.log('Google Sign-Up successful:', user);
    
            const response = await fetch('http://localhost:8000/users/signup/google', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ email: user.email, handicap: null })
            });
    
            if (response.ok) {
                const data = await response.json();
                console.log('Google sign up successful:', data);
                localStorage.setItem('access_token', await user.getIdToken());
                localStorage.setItem('email', user.email);
                setIsLoggedIn(true);
                onRequestClose();
                navigate('/dashboard');
            } else {
                const errorData = await response.json();
                console.error('SignUp failed:', errorData);
            }
        } catch (error) {
            console.error('Google Sign-Up failed:', error);
        }
    };

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            contentLabel="SignUp"
            className="modal"
            overlayClassName="overlay"
        >
            <div className="modal-content">
                <h2>Get Started</h2>
                <p>You can create an account using Google or email.</p>
                <button className="google-btn" onClick={handleGoogleSignUp}>Sign up with Google</button>
                <hr />
                <input
                    type="email"
                    placeholder="Enter your Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
                <input
                    type="password"
                    placeholder="Enter your Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
                <input
                    type="password"
                    placeholder="Re-enter your Password"
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                    required
                />
                <button onClick={handleSignup}>Create Account</button>
                <p className="status-message">{statusMessage}</p>
            </div>
        </Modal>
    );
};

export default SignUpModal;
