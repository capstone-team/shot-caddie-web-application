// import necessary modules
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Modal from 'react-modal';
import { auth, googleProvider, signInWithPopup } from '../../firebase';
import { useAuth } from '../AuthContext';
import './LoginModal.css';

// Set the root element for the modal
Modal.setAppElement('#root');

// LoginModal component
// This component is a modal that allows the user to log in using email or Google
const LoginModal = ({ isOpen, onRequestClose }) => {
    // Set the initial state of the user email and password
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    
    // Get the navigate function from the useNavigate hook
    const navigate = useNavigate();
    
    // Get the setIsLoggedIn and setHasSeenTip functions from the useAuth hook
    const { setIsLoggedIn, setHasSeenTip } = useAuth();

    const handleLogin = async () => {

        const payload = new URLSearchParams({ username: email, password: password });
        const response = await fetch('http://localhost:8000/users/login', {
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            body: payload
        });
        
        if (response.ok) {
            const data = await response.json();
            localStorage.setItem('access_token', data.access_token);
            localStorage.setItem('email', email);
            setIsLoggedIn(true);
            setHasSeenTip(false);  // Reset the flag
            onRequestClose();
            navigate('/dashboard');
        } else {
            console.error('Login failed');
        }
    };

    const handleGoogleSignIn = async () => {
        try {
            const result = await signInWithPopup(auth, googleProvider);
            const user = result.user;
            localStorage.setItem('access_token', await user.getIdToken());
            localStorage.setItem('email', user.email);
            setIsLoggedIn(true);
            setHasSeenTip(false);  // Reset the flag
            onRequestClose();
            navigate('/dashboard');
        } catch (error) {
            console.error('Google Sign-In failed:', error);
        }
    };

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            contentLabel="Login"
            className="modal"
            overlayClassName="overlay"
        >
            <div className="modal-content">
                <h2>Welcome Back</h2>
                <p>You can log into your account using Google or email.</p>
                <button className="google-btn" onClick={handleGoogleSignIn}>Sign in with Google</button>
                <hr />
                <input
                    type="email"
                    placeholder="Enter your Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                <input
                    type="password"
                    placeholder="Enter your Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                <button onClick={handleLogin}>Login</button>

            </div>
        </Modal>
    );
};

export default LoginModal;
