import React from 'react';
import Modal from 'react-modal';

// Make sure this is called once in your application
Modal.setAppElement('#root');

const GolfTipPopup = ({ isOpen, onRequestClose, tip }) => {
    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            contentLabel="Golf Tip"
            className="modal"
            overlayClassName="overlay"
        >
            <div className="modal-content">
                <h2>Golf Tip</h2>
                <p>{tip}</p>
                <button onClick={onRequestClose}>Close</button>
            </div>
        </Modal>
    );
};

export default GolfTipPopup;
