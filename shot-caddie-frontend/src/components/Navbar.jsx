import React, { useEffect } from 'react';
import './Navbar.css';
import LoginModal from './LoginAndSignUp/LoginModal';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from './AuthContext';

const Navbar = () => {
    const { isLoggedIn, setIsLoggedIn } = useAuth();
    const [isLoginModalOpen, setIsLoginModalOpen] = useState(false);
    const navigate = useNavigate();

    useEffect(() => {
        const token = localStorage.getItem('access_token');
        setIsLoggedIn(!!token);
    }, [setIsLoggedIn]);

    const handleLogout = () => {
        localStorage.removeItem('email');
        localStorage.removeItem('access_token');
        setIsLoggedIn(false);
        navigate('/');
    };

    return (
        <nav className="navbar">
            <div className="navbar-brand" id='navbar-brand'>Shot Caddie</div>
            <div className="navbar-buttons">
                {isLoggedIn ? (
                    <button className="btn-logout" onClick={handleLogout}>Logout</button>
                ) : (
                    <button className="btn-login" onClick={() => setIsLoginModalOpen(true)}>Login</button>
                )}
                <LoginModal
                    isOpen={isLoginModalOpen}
                    onRequestClose={() => setIsLoginModalOpen(false)}
                />
            </div>
        </nav>
    );
}

export default Navbar;
