// Import necessary libraries
import React, { useState, useEffect } from 'react';
import Sidebar from './SidebarNavigation/Sidebar';
import GolfTipPopup from './GolfTipPopUp';
import { useAuth } from './AuthContext'  // Import the useAuth hook
import './UserDashboard.css';

// UserDashboard component
// This component is the main user dashboard page that displays the user's email and handicap along with the sidebar navigation.
const UserDashboard = () => {

    // Get the user's email and token from local storage
    const userEmail = localStorage.getItem('email');
    const token = localStorage.getItem('access_token');

    // Set the initial state of the handicap, newHandicap, golfTip, isTipOpen
    const [handicap, setHandicap] = useState(null);
    const [golfTip, setGolfTip] = useState('');
    const [isTipOpen, setIsTipOpen] = useState(false);

    // Get the isLoggedIn, hasSeenTip, setHasSeenTip from the useAuth hook
    const { hasSeenTip, setHasSeenTip } = useAuth();
    

    // useEffect hook to fetch the user's handicap and a random golf tip on
    // component mount or when dependencies change
    useEffect(() => {
        if (userEmail) {
            // Fetch the user's handicap
            fetch(`http://localhost:8000/users/${userEmail}/handicap`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            })
                .then(response => response.json())
                .then(data => {
                    if (data.handicap !== undefined) {
                        setHandicap(data.handicap);
                    }
                })
                .catch(error => {
                    console.error('Error fetching handicap:', error);
                });

            // Fetch a random golf tip if not seen
            if (!hasSeenTip) {
                fetch(`http://localhost:8000/golf_tips/random`, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    }
                })
                    .then(response => response.json())
                    .then(data => {
                        setGolfTip(data);
                        setIsTipOpen(true);
                        setHasSeenTip(true);  // Mark as seen
                    })
                    .catch(error => {
                        console.error('Error fetching golf tip:', error);
                    });
            }
        }
    }, [userEmail, token, hasSeenTip, setHasSeenTip]);

    // Return the JSX to render
    return (
        <div className="dashboard-container">
            <Sidebar />
            <div className="content">
                <h2>Welcome to your dashboard, {userEmail}</h2>
                <div className="handicap-section">
                    <div className="handicap-display">
                        <p>Your current Handicap Index is:</p>
                        <br />
                        <div className="handicap-value">{handicap !== null ? handicap : 'N/A'}</div>
                        <p>Handicap</p>
                    </div>
                </div>
            </div>
            <GolfTipPopup isOpen={isTipOpen} onRequestClose={() => setIsTipOpen(false)} tip={golfTip} />
        </div>
    );
};

export default UserDashboard;
