import React, { useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2';
import { Chart as ChartJS, Legend, ArcElement } from 'chart.js'; 
import Sidebar from './Sidebar';
import './Statistics.css';

ChartJS.register(Legend, ArcElement);

const Statistics = () => {
  const [roundData, setRoundData] = useState([]);
  const [selectedRound, setSelectedRound] = useState(null);
  
  const [clubs, setClubs] = useState([]);
  const [stats, setStats] = useState({
    puttsPerHole: 0,
    gir: 0,
    birdies: 0,
    pars: 0,
    bogeys: 0,
    avgDrivingDistance: 0,
    longestDrive: 0,
    pieChartData: {
      labels: [],
      datasets: [
        {
          data: [],
          backgroundColor: [],
          hoverOffset: 4,
          responsive: true
        }
      ]
    }
  });
  const userEmail = localStorage.getItem('email');
  const token = localStorage.getItem('access_token');

  useEffect(() => {
    const fetchRounds = async () => {
      try {
        const response = await fetch(`http://localhost:8000/users/${userEmail}/round`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
          }
        });
        const data = await response.json();
        setRoundData(data);
      } catch (error) {
        console.error('Error fetching rounds:', error);
      }
    };

    fetchRounds();
  }, [userEmail, token]);

  useEffect(() => {
    const fetchClubs = async () => {
      try {
        const response = await fetch(`http://localhost:8000/users/${userEmail}/golf_clubs`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
          }
        });
        const data = await response.json();
        setClubs(data.clubs);
      } catch (error) {
        console.error('Error fetching clubs:', error);
      }
    };

    fetchClubs();
  }, [userEmail, token]);

  useEffect(() => {
    if (selectedRound) {
      calculateStatistics(selectedRound);
    }
  }, [selectedRound]);

  const handleRoundChange = (event) => {
    const [courseName, date] = event.target.value.split(' - ');
    const round = roundData.find(round => round.courseName === courseName && round.date === date);
    if (!round) {
      setSelectedRound(null);
    } else {
      setSelectedRound(round);
    }
  };

  const calculateStatistics = (round) => {
    const totalHoles = round.listOfGolfHoles.length;
    let totalPutts = 0;
    let girCount = 0;
    let birdiesCount = 0;
    let parsCount = 0;
    let bogeysCount = 0;
    let totalDistance = 0;
    let longestDrive = 0;
    let driverHole = 0;

    round.listOfGolfHoles.forEach(hole => {
      totalPutts += hole.numOfPutts;
      if (hole.holeScore <= hole.par - 1) {
        birdiesCount++;
      } else if (hole.holeScore === hole.par) {
        parsCount++;
      } else {
        bogeysCount++;
      }
      hole.listOfBalls.forEach(ball => {
        if (ball.shotNumber === 1 && ball.clubUsed === 'Driver') {
          driverHole++;
          totalDistance += ball.distance;
          if (ball.distance > longestDrive) {
            longestDrive = ball.distance;
          }
        }
      });
      if (hole.holeScore - hole.numOfPutts <= hole.par - 2) {
        girCount++;
      }
    });

    const puttsPerHole = totalPutts / totalHoles;
    const gir = (girCount / totalHoles) * 100;
    const avgDrivingDistance = totalDistance / driverHole;

    const pieChartData = {
      labels: [`Birdies ${birdiesCount}`, `Pars ${parsCount}`, `Bogeys ${bogeysCount}`],
      datasets: [
        {
          label: 'Birdies, Pars, and Bogeys',
          data: [birdiesCount, parsCount, bogeysCount],
          backgroundColor: [
            'rgb(81,174,252)',
            'rgb(0, 128, 0)',
            'rgb(255, 0, 0)'
          ],
          hoverOffset: 4,
          responsive: true
        },
      ],
    };
    

    setStats({
      puttsPerHole,
      gir,
      birdies: birdiesCount,
      pars: parsCount,
      bogeys: bogeysCount,
      avgDrivingDistance,
      longestDrive,
      pieChartData
    });

    
  };

  return (
    <div className="statistics-container">
      <h2>Statistics Page</h2>
      <p>Review Your Round and Lifetime Statistics</p>
      <Sidebar />
      {roundData.length > 0 && (
        <div>
          <select className="round-select" id="round-select" onChange={handleRoundChange} defaultValue="">
            <option value="">Select a Course and Date</option>
            {roundData.map((round, index) => (
              <option key={index} value={`${round.courseName} - ${round.date}`}>
                {round.courseName} - {round.date}
              </option>
            ))}
          </select>
        </div>
      )}
      {selectedRound && (
        <div className="stats-display">
          <div className="stat-item">
            <p> <strong>Average Number of Putts:</strong> {stats.puttsPerHole.toFixed(2)}</p>
          </div>
          <div className="stat-item">
            <p><strong>GIR (Greens in Regulation):</strong> {stats.gir.toFixed(1)}%</p>
          </div>
          <div className="stat-item">
            <p><strong>Driving Distance:</strong> {stats.avgDrivingDistance.toFixed(1)}y</p>
          </div>
          <div className="stat-item">
            <p><strong>Longest Drive:</strong> {stats.longestDrive}y</p>
          </div>
          <div className='stat-item'>
            <p><strong>Round Scores:</strong></p>
            <Pie data={stats.pieChartData} />
          </div>
        </div>
      )}
      <div className="clubs-display">
        <h3>Your Lifetime Golf Club Stats</h3>
        {Object.entries(clubs).map(([club, stats]) => (
            <div className="stat-item-clubs" key={club}>
              <p><strong>{club}</strong></p>
              <p>Average Distance: <strong>{stats.first}yd</strong></p>
            </div>
          ))}
      </div>
    </div>
  );
};

export default Statistics;
