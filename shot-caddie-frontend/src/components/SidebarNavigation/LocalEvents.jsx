
import React, { useState, useEffect } from 'react';
import Sidebar from './Sidebar';
import './LocalEvents.css';


const LocalEvents = () => {
    const [state, setState] = useState('');
    const [city, setCity] = useState('');
    const [events, setEvents] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);
    const [triggerSearch, setTriggerSearch] = useState(false);
  
    useEffect(() => {
      if (!triggerSearch) return;
  
      const fetchEvents = async () => {
        setIsLoading(true);
        setError(null);
  
        try {
          const response = await fetch(`http://localhost:8000/events?state=${state}&city=${city}`, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
            },
          });
          if (!response.ok) {
            throw new Error('Error fetching events');
          }
          const data = await response.json();
          setEvents(data);
        } catch (error) {
          setError(error.message);
        } finally {
          setIsLoading(false);
          setTriggerSearch(false);
        }
      };
  
      fetchEvents();
    }, [triggerSearch, state, city]);
  
    const handleSearch = () => {
      setTriggerSearch(true);
    };


    return (
        <div className="event-search-container">
        <Sidebar />
          <h2>Find Golf Events Near You</h2>
          <div className="input-container">
          <input
            className='city-state-input'
              type="text"
              placeholder="City"
              value={city}
              onChange={(e) => setCity(e.target.value)}
            />
            <input
            className='city-state-input'
              type="text"
              placeholder="State (Format: VA, CA, etc.)"
              value={state}
              onChange={(e) => setState(e.target.value)}
            />
            <button onClick={handleSearch}>Search</button>
          </div>
          {isLoading && <p>Loading events...</p>}
          {error && <p>Error: {error}</p>}
          {!error && events.length > 0 && (
            <div className="events-list">
              <h3>Events in {city}, {state}</h3>
              <ul>
                {events.map((event, index) => (
                  <li key={index}>
                    <strong>{event.title}</strong> - {event.date}<br />
                    <em>{event.location}</em>
                  </li>
                ))}
              </ul>
            </div>
          )}
        </div>
      );
    };
    
export default LocalEvents;
