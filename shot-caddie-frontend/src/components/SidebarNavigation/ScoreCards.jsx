// components/ScoreCards.jsx
import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import './ScoreCards.css';
import Sidebar from './Sidebar';


const ScoreCards = () => {
    const [courseId, setCourseId] = useState('');
    const [numberOfHoles, setNumberOfHoles] = useState(null);
    const [front9Score, setFront9Score] = useState('');
    const [back9Score, setBack9Score] = useState('');
    const [adjustedGrossScore, setAdjustedGrossScore] = useState('');
    const [scorecards, setScorecards] = useState([]);
    const [holes, setHoles] = useState([]);
    const [holeScores, setHoleScores] = useState({});
    const [showScorecardTable, setShowScorecardTable] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');
    const userEmail = localStorage.getItem('email');
    const token = localStorage.getItem('access_token');
    const navigate = useNavigate();

    const handleLogout = () => {
        localStorage.removeItem('email');
        localStorage.removeItem('access_token');
        navigate('/');
    };
    useEffect(() => {
        fetch('http://localhost:8000/scorecards', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then(response => response.json())
            .then(data => {
                if (Array.isArray(data)) {
                    setScorecards(data);
                } else {
                    console.error('Expected an array of scorecards, but got:', data);
                    setScorecards([]);
                }
            })
            .catch(error => {
                console.error('Error fetching scorecards:', error);
            });
    }, [token]);

    useEffect(() => {
        if (numberOfHoles === 18) {
            const front9 = parseInt(front9Score, 10);
            const back9 = parseInt(back9Score, 10);
            if (!isNaN(front9) && !isNaN(back9)) {
                setAdjustedGrossScore(front9 + back9);
            } else {
                setAdjustedGrossScore('');
            }
        }
    }, [front9Score, back9Score, numberOfHoles]);

    useEffect(() => {
        if (courseId) {
            fetch(`http://localhost:8000/holes/${courseId}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            })
                .then(response => response.json())
                .then(data => {
                    if (data) {
                        const sortedData = data.sort((a, b) => a.holeNumber - b.holeNumber);
                        setHoles(sortedData);
                    }
                })
                .catch(error => {
                    console.error('Error fetching holes:', error);
                });
        }
    }, [courseId, token]);

    const handleScoreChange = (holeNumber, score) => {
        const updatedScores = {
            ...holeScores,
            [holeNumber]: score
        };
        setHoleScores(updatedScores);

        let totalScore = 0;
        for (let holeNum = 1; holeNum <= 18; holeNum++) {
            totalScore += parseInt(updatedScores[holeNum], 10) || 0;
        }
        setAdjustedGrossScore(totalScore.toString());
    };

    const handleAddScorecard = async () => {
        const scorecard = {
            courseId: courseId,
            date_played: new Date().toISOString(),
            score: parseInt(adjustedGrossScore, 10),
            email: userEmail
        };

        const response = await fetch(`http://localhost:8000/users/${userEmail}/user_scorecard`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(scorecard)
        });

        if (response.ok) {
            const newScorecard = await response.json();
            setScorecards([...scorecards, newScorecard]);
            setSuccessMessage('Score Added to User Profile, Handicap Updated!');
            setTimeout(() => {
                navigate('/dashboard');
            }, 1000); // Navigate after 2 seconds
        } else {
            const errorData = await response.json();
            console.error('Failed to add scorecard:', errorData);
        }
    };

    const toggleScorecardTable = () => {
        setShowScorecardTable(!showScorecardTable);
    };

    const handleHolesChange = (holesCount) => {
        setNumberOfHoles(holesCount);
        setShowScorecardTable(false); // Hide scorecard table when switching between 9 and 18 holes
    };

    return (
        <div className='scorecards-container'>
            <Sidebar handleLogout={handleLogout} />
            <h2>Record Rounds</h2>
            <p>Here you can record a round at a golf course</p>
            <div>
                <select
                    value={courseId}
                    onChange={e => setCourseId(e.target.value)}
                >
                    <option value="">Select Course</option>
                    {scorecards.map(scorecard => (
                        <option key={scorecard.courseId} value={scorecard.courseId}>
                            {scorecard.courseName}
                        </option>
                    ))}
                </select>

                {courseId && (
                    <>
                        <div>
                            <button onClick={() => handleHolesChange(9)}>9 Holes</button>
                            <button onClick={() => handleHolesChange(18)}>18 Holes</button>
                            <button onClick={toggleScorecardTable}>View Full Scorecard</button>
                        </div>

                        {numberOfHoles === 9 && !showScorecardTable && (
                            <div>
                                <input
                                    type="number"
                                    placeholder="Adjusted Gross Score"
                                    value={adjustedGrossScore}
                                    onChange={e => setAdjustedGrossScore(e.target.value)} required
                                />
                            </div>
                        )}

                        {numberOfHoles === 18 && !showScorecardTable && (
                            <div>
                                <input
                                    type="number"
                                    placeholder="Front 9 Score"
                                    value={front9Score}
                                    onChange={e => setFront9Score(e.target.value)} required
                                />
                                <input
                                    type="number"
                                    placeholder="Back 9 Score"
                                    value={back9Score}
                                    onChange={e => setBack9Score(e.target.value)} required
                                />
                                <input
                                    type="number"
                                    placeholder="Adjusted Gross Score"
                                    value={adjustedGrossScore}
                                    readOnly
                                />
                            </div>
                        )}

                        {showScorecardTable && holes && (
                            <div className='scorecardtable'>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td colSpan={3}>Hole Info</td>
                                            <td>Tees</td>
                                            <td>Scores</td>
                                        </tr>
                                        <tr>
                                            <td>Hole</td>
                                            <td>Blue (Yards)</td>
                                            <td>Handicap</td>
                                            <td>Par</td>
                                            
                                        </tr>
                                        {holes.map(hole => (
                                            <tr key={hole.holeNumber} className='nonfocus'>
                                                <td>{hole.holeNumber}</td>
                                                <td bgcolor='#00ccff'>{hole.distance}</td>
                                                <td className='handicap-column'>{hole.handicap}</td>
                                                <td>{hole.par}</td>
                                                <td>
                                                    <input
                                                        type="number"
                                                        className='input'
                                                        value={holeScores[hole.holeNumber] || ''}
                                                        onChange={e => handleScoreChange(hole.holeNumber, e.target.value)} 
                                                        required
                                                    />
                                                </td>
                                            </tr>
                                        ))}
                                        <tr>
                                            <td>Total</td>
                                            <td>{holes.reduce((sum, hole) => sum + hole.distance, 0)}</td>
                                            <td>CR/Slope: </td>
                                            <td>{holes.reduce((sum, hole) => sum + hole.par, 0)}</td>
                                            <td>
                                                <input type="number" className='input'
                                                    placeholder="Adj"
                                                    value={adjustedGrossScore}
                                                    readOnly />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        )}

                        <button onClick={handleAddScorecard}>Add Scorecard</button>
                    </>
                )}
            </div>
        </div>
    );
};

export default ScoreCards;
