// components/TeeTimes.jsx
import React, { useState } from 'react';
import Sidebar from './Sidebar';
import './TeeTimes.css';


const courses = [
    { id: 1, name: "Greendale Golf Course", url: "https://fairfax-county-mco.book.teeitup.golf/?course=7742&date=${getCurrentDateFormatted}" },
    { id: 2, name: "Laurel Hill Golf Club", url: "https://fairfax-county-mco.book.teeitup.golf/?course=4595&date=${getCurrentDateFormatted}" },
    { id: 3, name: "Reston National Golf Club", url: "https://www.chronogolf.com/club/14474/widget?medium=widget&source=club" }
];


const TeeTimes = () => {
    const [selectedCourse, setSelectedCourse] = useState(courses[0].url);

    const handleCourseChange = (e) => {
        const selectedCourseId = parseInt(e.target.value, 10);
        const course = courses.find(course => course.id === selectedCourseId);
        setSelectedCourse(course.url);
    };

    return (
        <div>
            <Sidebar />
            <div className="tee-times-container">
                <h2>TeeTimes Page</h2>
                <p>Here you can book tee times.</p>
                <div>
                    <select onChange={handleCourseChange}>
                        {courses.map(course => (
                            <option  key={course.id} value={course.id}>{course.name}</option>
                        ))}
                    </select>
                </div>
                <div className='teetimes-box'>
                    <embed src={selectedCourse} type="" height={1000} width={1500}/>
                </div>
                
            </div>
        </div>
    );
};

export default TeeTimes;
