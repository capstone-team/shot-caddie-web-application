import React from "react";
import { NavLink } from "react-router-dom";
import { FaChartBar, FaUsers, FaGolfBall, FaFlag } from 'react-icons/fa';
import { MdSpaceDashboard } from "react-icons/md";
import './Sidebar.css';


const Sidebar = () => {
    return (
        <div className="sidebar">
            <div className="menu">
                <ul>
                    <li>
                        <NavLink to="/dashboard" activeClassName="active">
                            <MdSpaceDashboard /> Dashboard
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/statistics" activeClassName="active">
                            <FaChartBar /> Statistics
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/localevents" activeClassName="active">
                            <FaUsers /> Local Events
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/teetimes" activeClassName="active">
                            <FaFlag /> Book Tee Time
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/scorecards" activeClassName="active">
                            <FaGolfBall /> Record Round
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/reviewrounds" activeClassName="active">
                            <FaChartBar />Review Rounds
                        </NavLink>
                    </li>
                </ul>
            </div>
        </div>
    )

};

export default Sidebar;