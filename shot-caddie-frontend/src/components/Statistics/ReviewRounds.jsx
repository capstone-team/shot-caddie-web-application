import React, { useState, useEffect } from 'react';
import Sidebar from '../SidebarNavigation/Sidebar';
import MapComponent from './MapComponent';
import RoundDetails from './RoundDetails';


const ReviewRounds = () => {
  const [roundData, setRoundData] = useState([]);
  const [selectedRound, setSelectedRound] = useState(null);
  const [currentHoleIndex, setCurrentHoleIndex] = useState(0);
  const userEmail = localStorage.getItem('email');
  const token = localStorage.getItem('access_token');

  useEffect(() => {
    const fetchRounds = async () => {
      try {
        const response = await fetch(`http://localhost:8000/users/${userEmail}/round`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
          }
        });
        const data = await response.json();
        setRoundData(data);
      } catch (error) {
        console.error('Error fetching rounds:', error);
      }
    };

    fetchRounds();
  }, [userEmail, token]);

  const handleRoundChange = (event) => {
    const [courseName, date] = event.target.value.split(' - ');
    const round = roundData.find(round => round.courseName === courseName && round.date === date);
    if (!round) {
      setSelectedRound(null);
    } else {
      setSelectedRound(round);
      setCurrentHoleIndex(0); 
    }
    
  };

  return (
    <div className="statistics-container">
      <h2>Review Rounds Page</h2>
      <p>Review Your Round and Lifetime Statistics</p>
      <Sidebar />
      {roundData.length > 0 && (
        <div>
          <select id="round-select" onChange={handleRoundChange} defaultValue="">
          <option value="">Select a Course and Date</option>
            {roundData.map((round, index) => (
              <option key={index} value={`${round.courseName} - ${round.date}`}>
                {round.courseName} - {round.date}
              </option>
            ))}
          </select>
        </div>
      )}
      {selectedRound && (
        <>
          <MapComponent
            selectedRound={selectedRound}
            currentHoleIndex={currentHoleIndex}
            setCurrentHoleIndex={setCurrentHoleIndex}
          />
        </>
      )}
    </div>
  );
};

export default ReviewRounds;
