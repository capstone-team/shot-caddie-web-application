import React from 'react';
import './RoundDetails.css';

const RoundDetails = ({ selectedRound, currentHoleIndex }) => {
  if (!selectedRound) {
    return <div>Loading...</div>;
  }

  const currentHole = selectedRound.listOfGolfHoles[currentHoleIndex];

  return (
    <div>
      <h2>Round Details</h2>
      <p>Course: {selectedRound.courseName}</p>
      <p>Date: {selectedRound.date}</p>
      <p>Hole: {currentHoleIndex + 1}</p>
      <ul className='shot-list'>
        {currentHole.listOfBalls.map((ball, index) => (
          <li key={index}>
            Shot {ball.shotNumber}: {ball.clubUsed}, Distance: {ball.distance}
          </li>
        ))}
        <br />
        <li>Number of Putts: {currentHole.numOfPutts}</li>
      </ul>
    </div>
  );
};

export default RoundDetails;
