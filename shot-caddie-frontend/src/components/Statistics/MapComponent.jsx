import React, { useState, useEffect } from 'react';
import { GoogleMap, useLoadScript, Marker } from '@react-google-maps/api';
import './MapComponent.css';

const MapComponent = ({ selectedRound, currentHoleIndex, setCurrentHoleIndex }) => {
  const [holes, setHoles] = useState([]);
  const [center, setCenter] = useState(null);
  const [ballMarks, setBallMarks] = useState([]);
  const token = localStorage.getItem('access_token');
  const courseId = 1;

  const { isLoaded } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAPS_API_KEY
  });

  useEffect(() => {
    const fetchHoles = async () => {
      try {
        const response = await fetch(`http://localhost:8000/holes/${courseId}/holes`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
          }
        });
        const data = await response.json();
        const sortedHoles = data.sort((a, b) => a.holeNumber - b.holeNumber);
        setHoles(sortedHoles);
        if (sortedHoles.length > 0) {
          setCurrentHoleIndex(0);
        }
      } catch (error) {
        console.error('Error fetching holes:', error);
      }
    };

    fetchHoles();
  }, [courseId, token, setCurrentHoleIndex]);

  useEffect(() => {
    if (holes.length > 0) {
      const currentHole = holes[currentHoleIndex];
      const lat = (currentHole.northEastBound.latitude + currentHole.southWestBound.latitude) / 2;
      const lng = (currentHole.northEastBound.longitude + currentHole.southWestBound.longitude) / 2;
      setCenter({ lat, lng });
    }
  }, [holes, currentHoleIndex]);

  useEffect(() => {
    if (selectedRound && selectedRound.listOfGolfHoles.length > 0) {
      const currentHoleMarks = selectedRound.listOfGolfHoles.find(
        (hole) => hole.holeNumber === holes[currentHoleIndex]?.holeNumber
      );
      setBallMarks(currentHoleMarks ? currentHoleMarks.listOfBalls : []);
    }
  }, [selectedRound, currentHoleIndex, holes]);

  const handlePreviousHole = () => {
    setCurrentHoleIndex((prevIndex) => (prevIndex > 0 ? prevIndex - 1 : holes.length - 1));
  };

  const handleNextHole = () => {
    setCurrentHoleIndex((prevIndex) => (prevIndex < holes.length - 1 ? prevIndex + 1 : 0));
  };

  if (!isLoaded) {
    return <div>Loading...</div>;
  }

  const currentHole = selectedRound.listOfGolfHoles[currentHoleIndex];

  return (
    <div className='map-container'>
      <p>Greendale Golf Course - Hole {holes[currentHoleIndex]?.holeNumber}</p>
      <div className='round-details'>
        {center && (
          <GoogleMap
            mapContainerStyle={{ width: 700, height: 600, margin: 'auto' }}
            center={center}
            zoom={17.4}
            options={{
              mapTypeId: 'satellite',
              mapTypeControl: false,
              streetViewControl: false,
              fullscreenControl: false,
              zoomControl: true,
              zoomControlOptions: {
                position: window.google.maps.ControlPosition.RIGHT_BOTTOM
              }
            }}
          >
            {holes.length > 0 && ballMarks.map((mark, index) => (
              <Marker
                key={index}
                position={{ lat: mark.location.latitude, lng: mark.location.longitude }}
                label={`${mark.shotNumber}`}
              />
            ))}
          </GoogleMap>
        )}
      </div>
      <div className='round-details'>
        <button className='round-button' onClick={handlePreviousHole}>Previous Hole</button>
        <button className='round-button' onClick={handleNextHole}>Next Hole</button>
        <h2>Round Details</h2>
      <p>Course: {selectedRound.courseName}</p>
      <p>Date: {selectedRound.date}</p>
      <p>Hole: {currentHoleIndex + 1}</p>
      <ul className='shot-list'>
        {currentHole.listOfBalls.map((ball, index) => (
          <li key={index}>
            Shot {ball.shotNumber}: {ball.clubUsed}, Distance: {ball.distance}
          </li>
        ))}
        <br />
        <li>Number of Putts: {currentHole.numOfPutts}</li>
      </ul>
      </div>
    </div>
  );
};

export default MapComponent;
