// App.js
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Navbar from './components/Navbar';
import Welcome from './components/Welcome';
import UserDashboard from './components/UserDashboard';
import Statistics from './components/SidebarNavigation/Statistics';
import LocalEvents from './components/SidebarNavigation/LocalEvents';
import TeeTimes from './components/SidebarNavigation/TeeTimes';
import ScoreCards from './components/SidebarNavigation/ScoreCards';
import ReviewRounds from './components/Statistics/ReviewRounds';
  // Import the LoginModal component
import './App.css';
function App() {
    
    return (
        <Router>
            <div className="App">
                <Navbar className='navbar'/>
                <Routes>
                    <Route path="/" element={<Welcome />} />
                    <Route path="/dashboard" element={<UserDashboard />} />
                    <Route path="/statistics" element={<Statistics />} />
                    <Route path="/localevents" element={<LocalEvents />} />
                    <Route path="/teetimes" element={<TeeTimes />} />
                    <Route path="/scorecards" element={<ScoreCards />} />
                    <Route path="/reviewrounds" element={<ReviewRounds />} />
                    
                </Routes>
            </div>
        </Router>
    );
}

export default App;
