import firebase_admin
from fastapi import FastAPI, HTTPException, Depends
from firebase_admin import credentials, firestore, initialize_app, auth
from app.web.routers import user_routes, scorecard_routes, holes_routes, events_routes, golf_tips_routes
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

origins = [
    "http://localhost:3000",  # React app address
    # add any other origins that need access
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],  # Allows all methods, you can change this to the specific methods you need
    allow_headers=["*"],
)


app.include_router(user_routes.router, prefix="/users")
app.include_router(scorecard_routes.router, prefix="/scorecards")
app.include_router(holes_routes.router, prefix="/holes")
app.include_router(events_routes.router, prefix="/events")
app.include_router(golf_tips_routes.router, prefix="/golf_tips")

@app.get("/")
def read_root():
    return {"message": "Welcome to the ShotCaddie API"}




