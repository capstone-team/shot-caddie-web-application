from typing import Optional
from pydantic import BaseModel
from datetime import datetime
from .holes_models import GeoPoint
from typing import List
from .user_schemas import UserScorecard

class User(BaseModel):
    email: str
    password: str
    guid: str
    handicap: Optional[float] = None
    scores: Optional[List[UserScorecard]] = []
    handicapDifferentials: Optional[List[float]] = []
    listOfClubs: Optional[dict] = {}

class BallMarkerInformation(BaseModel):
    clubUsed: str
    dateTime: str
    distance: int
    holeNumber: int
    location: GeoPoint
    shotNumber: int

class RoundInformation(BaseModel):
    holeNumber: int
    holeScore: int                                                                                                                                                                                      
    listOfBalls: List[BallMarkerInformation]
    numOfPutts: int
    par: int

class UserRoundData(BaseModel):
    courseName: str
    date: str
    listOfGolfHoles: List[RoundInformation]
    listOfHoleScores: List[int]
    roundScore: int

class GolfTips(BaseModel):
    tip: str










