from firebase_admin import auth, firestore, initialize_app
from firebase_admin.exceptions import FirebaseError
from firebase_admin.credentials import Certificate
from app.core.config import settings
from app.data.scorecard_models import Courses
from typing import List
from app.data.scorecard_models import Scorecard
import os
import logging

db = firestore.client()

def get_scorecard(courseId: str) -> Scorecard:
    """
    Get a scorecard from the database
    """
    try:
        scorecard_ref = db.collection('scorecards').document(courseId).get()
        if scorecard_ref.exists:
            return Scorecard(**scorecard_ref.to_dict())
        else:
            return None
    except FirebaseError as e:
        logging.error(f'Error getting scorecard: {e}')
        return None


def create_scorecard(scorecard: Scorecard) -> Scorecard:
    """
    Create a scorecard in the database. Firestore generates a UID for the scorecard.
    """
    try:
        # Add the scorecard to the collection without specifying a document ID.
        # Firestore generates an auto ID.
        _, doc_ref = db.collection('scorecards').add(scorecard.dict())
        
        # Update the scorecard object with the generated ID.
        scorecard.id = doc_ref.id  # Assuming 'id' is the attribute in your Scorecard model to store the document ID.
        
        print(f'Scorecard ID: {doc_ref.id}')
        return scorecard
    except FirebaseError as e:
        logging.error(f'Error creating scorecard: {e}')
        return None
    



def fetch_scorecards() -> List[Scorecard]:
    logging.info("Starting to fetch scorecards from the database")
    try:
        scorecards = db.collection('scorecards').stream()
        print(f"Scorecards: {scorecards}")
        scorecard_list = [Scorecard(**scorecard.to_dict()) for scorecard in scorecards]
        logging.info(f"Successfully fetched {len(scorecard_list)} scorecards")
        return scorecard_list
    except FirebaseError as e:
        logging.error(f"Error fetching scorecards: {e}")
        return None

