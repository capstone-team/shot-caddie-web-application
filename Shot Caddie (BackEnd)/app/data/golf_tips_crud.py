from firebase_admin import auth, firestore, initialize_app
from firebase_admin.exceptions import FirebaseError
from firebase_admin.credentials import Certificate
from app.core.config import settings
from app.data.golf_tips_models import Tips
import os
import logging

db = firestore.client()


def get_golf_tips():
    try:
        tips_ref = db.collection('tips').document('golf_tips')
        tips_doc = tips_ref.get()
        if tips_doc.exists:
            tips_list = tips_doc.to_dict().get('tip', [])
            print(tips_list)
            return tips_list
        else:
            logging.error("No such document!")
            return []
    except FirebaseError as e:
        logging.error(f"Error getting golf tips: {e}")
        return []