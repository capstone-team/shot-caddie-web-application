from pydantic import BaseModel
from typing import Optional


class GeoPoint(BaseModel):
    longitude: float
    latitude: float

class Holes(BaseModel):
    courseId: int
    distance: int
    holeNumber: int
    handicap: Optional[int]
    par: int

class MapHoles(BaseModel):
    holeNumber: int
    northEastBound: Optional[GeoPoint]
    southWestBound: Optional[GeoPoint]

