from pydantic import BaseModel
from typing import Optional
from typing import List
from .scorecard_models import Holes
from google.cloud.firestore_v1 import GeoPoint
from app.utils.utils import serialize_geopoint

class ScorecardCreate(BaseModel):
    id: Optional[str] = None
    courseId: str
    holes: List[Holes]
    courseName: str