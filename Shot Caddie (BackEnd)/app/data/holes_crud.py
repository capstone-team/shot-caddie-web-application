from firebase_admin import firestore
from app.data.holes_models import Holes, GeoPoint, MapHoles
from typing import List
from firebase_admin.exceptions import FirebaseError
import logging

db = firestore.client()

def get_holes(courseId: int) -> List[Holes]:
    """
    Get holes from the database where the courseID matches the courseID passed in.
    """
    try:
        logging.info(f'Querying holes for courseId: {courseId}')
        holes_query = db.collection('holes').where('courseId', '==', courseId).stream()
        holes_list = list(holes_query)
        logging.info(f'Query result count: {len(holes_list)}')
        holes = [Holes(**hole.to_dict()) for hole in holes_list]
        logging.info(f'Holes: {holes}')
        return holes
    except FirebaseError as e:
        logging.error(f'Error getting holes: {e}')
        return []

def get_hole_bounds(courseId: int) -> List[dict]:
    try:
        holes_query = db.collection('holes').where('courseId', '==', courseId).stream()
        holes = []
        for hole in holes_query:
            hole_data = hole.to_dict()
            northEastBound = hole_data.get('northEastBound')
            southWestBound = hole_data.get('southWestBound')
            hole_info = {
                'holeNumber': hole_data['holeNumber'],
                'northEastBound': {'latitude': northEastBound.latitude, 'longitude': northEastBound.longitude} if northEastBound else None,
                'southWestBound': {'latitude': southWestBound.latitude, 'longitude': southWestBound.longitude} if southWestBound else None,
            }
            holes.append(hole_info)
        logging.info(f'Hole bounds: {holes}')
        return holes
    except FirebaseError as e:
        logging.error(f"Error fetching hole bounds: {str(e)}")
        raise e
