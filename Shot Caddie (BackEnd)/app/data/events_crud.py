# event_crud.py
import requests
from bs4 import BeautifulSoup

def fetch_events(state: str, city: str):
    url = f'https://gofindgolf.com/find/tournaments/{state}/{city}'
    print(url)
    response = requests.get(url)
    if response.status_code != 200:
        raise Exception(f"Failed to fetch events: {response.status_code}")
    
    soup = BeautifulSoup(response.text, 'html.parser')
    events = []

    for event in soup.find_all('div', class_='tcard d-flex flex-wrap'):
        title = event.find('span', class_='tname').text.strip()
        location = event.find('h6', class_='mb-0 print-cname').text.strip()
        date_elements = event.find_all('div', class_='align-self-end')
        date = date_elements[1].text.strip() if len(date_elements) > 1 else ""

        events.append({
            'title': ' '.join(title.split()),
            'location': ' '.join(location.split()),
            'date': ' '.join(date.split())
        })

    return events
