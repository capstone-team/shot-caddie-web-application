from firebase_admin import auth, firestore, initialize_app
from firebase_admin.exceptions import FirebaseError
from firebase_admin.credentials import Certificate
from passlib.hash import bcrypt
from app.core.config import settings
from app.data.user_schemas import UserReadGoogle
from app.data.user_schemas import UserScorecard
from app.data.user_models import User
import os
import logging
# Initialize the Firebase Admin SDK
cred = Certificate('app/config/shot-caddie-firebase-adminsdk-ocpp3-c952c306aa.json')
initialize_app(cred)

db = firestore.client()

def calculate_handicap_differential(score: int, course_rating: float, slope: float):
    return ((score - course_rating) * 113) / slope

def calculate_handicap_index(differentials):
    if len(differentials) < 20:
        num_scores = len(differentials)
    else:
        num_scores = 20
    
    lowest_differentials = sorted(differentials)[:min(num_scores, 10)]
    average = sum(lowest_differentials) / len(lowest_differentials)
    return round(average * 0.96, 1)

def create_user(email: str, password: str, handicap: float = None):
    user_record = None
    try:
        # Hash the password before storing it
        hashed_password = bcrypt.hash(password)
        
        # Create user in Firebase Authentication
        user_record = auth.create_user(
            email=email,
            password=hashed_password
        )

        user_data = {
            'email': email,
            'password': hashed_password,  # Store the hashed password
            'handicap': handicap if handicap is not None else None,
            'created_at': firestore.SERVER_TIMESTAMP,
            'guid': user_record.uid  # Set the guid to Firebase user uid
        }
        
        # Store user in Firestore
        db.collection('users').document(user_record.uid).set(user_data)
        return user_record

    except FirebaseError as e:
        logging.error(f"FirebaseError during user creation: {e}")
        # If an error occurs and user_record is set, attempt to delete the user
        if user_record:
            auth.delete_user(user_record.uid)
        raise e

    except Exception as e:
        logging.error(f"Unexpected error during user creation: {e}")
        # Cleanup if user_record was created
        if user_record:
            auth.delete_user(user_record.uid)
        raise e
    
def create_user_from_google(user_data: UserReadGoogle):
    try:
        user = auth.get_user_by_email(user_data.email)
        user_data_dict = {
            'email': user.email,
            'handicap': user_data.handicap if user_data.handicap is not None else None,
            'created_at': firestore.SERVER_TIMESTAMP,
            'guid': user.uid  # Use Firebase UID directly
        }
        db.collection('users').document(user.uid).set(user_data_dict)
        logging.info(f"User {user.email} stored in Firestore with UID: {user.uid}")
        # Return the user_data_dict including handicap
        return user_data_dict
    except FirebaseError as e:
        logging.error(f"FirebaseError: {e}")
        raise e
    except Exception as e:
        logging.error(f"Unexpected error: {e}")
        raise e


def get_user_by_email(email: str):
    try:
        user = auth.get_user_by_email(email)
        user_doc = db.collection('users').document(user.uid).get()
        if user_doc.exists:
            user_data = user_doc.to_dict()
            return user, user_data
        else:
            raise FirebaseError('User not found')
    except FirebaseError as e:
        raise e
    
def update_user_handicap(email: str, handicap: float):
    try:
        user = auth.get_user_by_email(email)
        user_ref = db.collection('users').document(user.uid)
        user_ref.update({'handicap': handicap})
        return user.uid, user.email, handicap
    except FirebaseError as e:
        raise e
    
def get_user_handicap(email: str):
    try:
        user, user_data = get_user_by_email(email)
        return user, user_data['handicap']
    except FirebaseError as e:
        raise e
    
def save_user_scorecard(email: str, user_scorecard_data: UserScorecard) -> UserScorecard:
    try:
        user, _ = get_user_by_email(email)
        user_scorecard_id = f"{user.uid}_{user_scorecard_data.courseId}_{user_scorecard_data.date_played}"
        user_scorecard_dict = user_scorecard_data.model_dump()
        user_scorecard_dict['id'] = user_scorecard_id
        user_scorecard_dict['guid'] = user.uid

        user_doc_ref = db.collection('users').document(user.uid)
        user_doc = user_doc_ref.get()
        if user_doc.exists:
            user_data = user_doc.to_dict()
            if 'scorecards' in user_data and isinstance(user_data['scorecards'], list):
                user_data['scorecards'].append(user_scorecard_dict)
            else:
                user_data['scorecards'] = [user_scorecard_dict]
            user_doc_ref.set(user_data)
        else:
            logging.error(f"User {email} not found")
            raise ValueError(f"User {email} not found")

        return UserScorecard(**user_scorecard_dict)
    except Exception as e:
        logging.error(f"Error saving user scorecard: {str(e)}")
        raise e
    

def update_user_handicap_index(email: str):
    _ , user_data = get_user_by_email(email)
    user_doc_ref = db.collection('users').document(user_data.get('guid'))
    print(f"User data: {user_doc_ref.get().to_dict()}")
    user_doc = user_doc_ref.get()
    if user_doc.exists:
        user_data = user_doc.to_dict()
        scorecards = user_data.get('scorecards', [])
        
        # Calculate differentials
        differentials = []
        for scorecard in scorecards:
            differential = calculate_handicap_differential(
                scorecard['score'], 
                scorecard['course_rating'], 
                scorecard['slope']
            )
            differentials.append(differential)
        
        # Calculate handicap index
        handicap_index = calculate_handicap_index(differentials)
        print(f"Handicap Index: {handicap_index}")
        
        # Update user's handicap index
        user_doc_ref.update({'handicap': handicap_index})
    else:
        print(f"User {email} not found")


def get_user_rounds(email: str):
    try:
        user, user_data = get_user_by_email(email)
        user_doc_ref = db.collection('users').document(user.uid)
        round_ref = user_doc_ref.collection('Greendale Golf Course').stream()
        
        rounds = []
        for round in round_ref:
            rounds.append(round.to_dict())
        return rounds
    except FirebaseError as e:
        raise e
    except Exception as e:
        raise e
    

def get_user_club_distances(email: str) -> dict:
    try:
        user, user_data = get_user_by_email(email)
        user_doc_ref = db.collection('users').document(user.uid)
        clubs_list = user_doc_ref.get().to_dict().get('listOfClubs', [])
        return {'clubs': clubs_list}
    except FirebaseError as e:
        print(f"FirebaseError: {e}")
        raise e
    except Exception as e:
        raise e





