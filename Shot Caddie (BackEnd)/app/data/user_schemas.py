from pydantic import BaseModel
from typing import Optional
from typing import List
from datetime import datetime

class Scorecard(BaseModel):
    id: str
    user_id: str
    course_id: str
    teebox_type: str
    date_played: datetime
    score: int

class UserCreate(BaseModel):
    email: str
    password: str
    handicap: Optional[float] = None

class UserReadGoogle(BaseModel):
    email: str
    handicap: Optional[float] = None

class UserRead(BaseModel):
    guid: str
    email: str
    handicap: Optional[float] = None
    scores: Optional[List[Scorecard]] = []

class UserUpdate(BaseModel):
    handicap: Optional[float] = None

class Token(BaseModel):
    access_token: str
    token_type: str

class HoleScore(BaseModel):
    holeNumber: int
    score: Optional[int] = None

class UserScorecard(BaseModel):
    id: Optional[str] = None
    email: str
    courseId: str
    date_played: datetime
    score: int
    slope: Optional[float] = 126.0
    course_rating: Optional[float] = 70.1
