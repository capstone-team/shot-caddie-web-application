from typing import Optional, List
from pydantic import BaseModel
from app.data.holes_models import Holes


class Scorecard(BaseModel):
    id: Optional[str] = None
    courseName: str
    courseId: str
    holes: List[Holes]
    slope: float
    courseRating: float

class Courses(BaseModel):
    courseId: str
    distance: int
    name: str
    par: int

