import requests
from bs4 import BeautifulSoup


url = 'https://gofindgolf.com/find/tournaments/VA/Arlington'
response = requests.get(url);
soup = BeautifulSoup(response.text, 'html.parser')


events = []

for event in soup.find_all('div', class_='tcard d-flex flex-wrap'):
    title = event.find('span', class_='tname').text.strip()
    location = event.find('h6', class_='mb-0 print-cname').text.strip()
    date = event.find_all('div', class_='align-self-end')
    date = date[1].text.strip()
    events.append({
        'title': ' '.join(title.split()),
        'location': ' '.join(location.split()),
        'date': ' '.join(date.split())
    })

print(events)
