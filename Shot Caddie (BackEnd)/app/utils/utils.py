from google.cloud.firestore_v1 import GeoPoint

def serialize_geopoint(geopoint: GeoPoint):
    return {"latitude": geopoint.latitude, "longitude": geopoint.longitude}


def calculate_handicap_differential(score: int, course_rating: float, slope: float):
    return ((score - course_rating) * 113) / slope

def calculate_handicap_index(differentials):
    if len(differentials) < 20:
        num_scores = len(differentials)
    else:
        num_scores = 20
    
    lowest_differentials = sorted(differentials)[:min(num_scores, 10)]
    average = sum(lowest_differentials) / len(lowest_differentials)
    return round(average * 0.96, 1)