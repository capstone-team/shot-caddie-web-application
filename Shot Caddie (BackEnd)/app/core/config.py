import os
from dotenv import load_dotenv

load_dotenv()

class Settings:
    PROJECT_NAME: str = "ShotCaddie"
    SECRET_KEY: str = os.getenv("SECRET_KEY")
    FIREBASE_CERTIFICATE_PATH: str = os.getenv("FIREBASE_CERTIFICATE_PATH")

settings = Settings()
