import logging
from fastapi import APIRouter, HTTPException
from app.services import holes_service
from app.data.scorecard_models import Holes
from typing import List

router = APIRouter()

@router.get("/{courseId}", response_model=List[Holes])
async def fetch_holes(courseId: int):
    try:
        logging.info(f"Fetching holes for course: {courseId}")
        return holes_service.fetch_holes(courseId)
    except ValueError as e:
        raise HTTPException(status_code=404, detail=str(e))
    

@router.get("/{courseId}/holes", response_model=List[dict])
async def get_hole_bounds(courseId: int):
    try:
        return holes_service.fetch_hole_bounds(courseId)
    except HTTPException as e:
        logging.error(f"Error fetching hole bounds: {e.detail}")
        raise e
    except Exception as e:
        logging.error(f"Unexpected error fetching hole bounds: {str(e)}")
        raise HTTPException(status_code=500, detail="An unexpected error occurred")
