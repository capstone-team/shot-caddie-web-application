from fastapi import APIRouter, Depends, HTTPException, Query
from app.services import user_service
from app.data.scorecard_models import Scorecard, Holes
from app.data.scorecard_schemas import ScorecardCreate
from app.services import scorecard_service
from app.core.security import create_access_token, verify_token
from typing import List
import logging

router = APIRouter()

@router.get("/{courseId}", response_model=Scorecard)
async def fetch_scorecard(courseId: str):
    try:
        logging.info(f"Fetching scorecard for course: {courseId}")
        return scorecard_service.fetch_scorecard(courseId)
    except ValueError as e:
        raise HTTPException(status_code=404, detail=str(e))
    
@router.post("/", response_model=ScorecardCreate)
async def create_scorecard(scorecard: Scorecard):
    try:
        logging.info(f"Creating scorecard for course: {scorecard.courseId}")
        return scorecard_service.create_scorecard(scorecard)
    except ValueError as e:
        raise HTTPException(status_code=400, detail=str(e))
    
@router.get("/", response_model=List[Scorecard])
async def fetch_scorecards():
    try:
        logging.info("Fetching all scorecards")
        scorecards = scorecard_service.fetch_scorecards()
        if scorecards is None:
            logging.info("No scorecards found, returning an empty list.")
            return []
        logging.debug(f"Scorecards fetched: {scorecards}")
        return scorecards
    except ValueError as e:
        logging.error(f"Error fetching scorecards: {e}")
        raise HTTPException(status_code=404, detail=str(e))
    

