from fastapi import APIRouter, Depends, HTTPException, Query
from app.services import user_service
from app.data.user_schemas import UserCreate, UserRead, Token, UserReadGoogle, UserScorecard
from app.data.user_models import UserRoundData
from app.services import golf_tips_service
from typing import List
import bcrypt 
from fastapi.security import OAuth2PasswordRequestForm, OAuth2PasswordBearer
from app.core.security import create_access_token, verify_token
from random import choice
import logging

router = APIRouter()

@router.post("/signup", response_model=UserRead)
async def signup(user: UserCreate):
    try:
        user_record = user_service.create_user(user)
        return {"guid": user_record.uid, "email": user_record.email, "handicap": user.handicap}
    except HTTPException as e:
        logging.error(f"Error during signup: {e.detail}")
        raise e
    except Exception as e:
        logging.error(f"Unexpected error during signup: {str(e)}")
        raise HTTPException(status_code=500, detail="An unexpected error occurred")
    
@router.post("/signup/google", response_model=UserReadGoogle)
async def signup_google(user: UserReadGoogle):
    try:
        logging.info(f"Received user data: {user}")
        user_record = user_service.create_user_from_google(user)
        logging.info(f"User created: {user_record}")
        return {"guid": user_record['guid'], "email": user_record['email'], "handicap": user_record['handicap']}
    except HTTPException as e:
        logging.error(f"Error during Google signup: {e.detail}")
        raise e
    except Exception as e:
        logging.error(f"Unexpected error during Google signup: {str(e)}")
        raise HTTPException(status_code=500, detail="An unexpected error occurred")


@router.post("/login", response_model=Token)
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    logging.info(f"Form data: {form_data}")
    user, user_data = user_service.get_user_by_email(form_data.username)

    # check password
    # if not user_data or not bcrypt.checkpw(form_data.password.encode('utf-8'), user_data.get('password').encode('utf-8')):
    #     raise HTTPException(status_code=400, detail="Incorrect email or password")
    
    # create the access token
    access_token = create_access_token(data={"sub": user.email})
    return {"access_token": access_token, "token_type": "bearer"}

@router.get("/me", response_model=UserRead)
async def read_users_me(token: str = Depends(verify_token)):
    user_email = verify_token(token)
    user, user_data = user_service.get_user_by_email(user_email)
    return {"guid": user.uid, "email": user.email, "handicap": user_data.get('handicap')}

@router.get("/{email}", response_model=UserRead)
async def read_user(email: str):
    user, user_data = user_service.get_user_by_email(email)
    logging.info(f"User fetched: {email}")
    return {"guid": user.uid, "email": user.email, "handicap": user_data.get('handicap')}

@router.put("/{email}/handicap", response_model=UserRead)
async def update_handicap(email: str, handicap: float = Query(...)):
    user_id, user_email, updated_handicap = user_service.update_handicap(email, handicap)
    return {"guid": user_id, "email": user_email, "handicap": updated_handicap}

@router.get("/{email}/handicap", response_model=UserRead)
async def get_handicap(email: str):
    user, handicap = user_service.get_user_handicap(email)
    logging.info(f"Handicap fetched for user: {email}, Handicap: {handicap}")
    return {"guid": user.uid, "email": user.email, "handicap": handicap}


@router.post("/{email}/user_scorecard", response_model=UserScorecard)
async def add_scorecard(email: str, scorecard: UserScorecard):
    try:
        user_scorecard = user_service.add_user_scorecard(email, scorecard)
        logging.info(f"Scorecard added for user: {email}, Course: {scorecard.courseId}, Scores: {scorecard.score}")
        return user_scorecard
    except HTTPException as e:
        logging.error(f"Error adding scorecard: {e.detail}")
        raise e
    except Exception as e:
        logging.error(f"Unexpected error adding scorecard: {str(e)}")
        raise HTTPException(status_code=500, detail="An unexpected error occurred")
    
@router.get("/{email}/round", response_model=List[UserRoundData])
async def get_rounds(email: str):
    try:
        user_rounds = user_service.get_user_round(email)
        logging.info(f"Rounds fetched for user: {email}")
        logging.info(f"Rounds: {user_rounds}")
        return user_rounds
    except HTTPException as e:
        logging.error(f"Error fetching rounds: {e.detail}")
        raise e

@router.get("/{email}/golf_clubs")
async def get_user_golf_clubs(email: str):
    logging.info(f"Fetching golf clubs for user: {email}")
    list_of_clubs = user_service.get_user_list_of_clubs(email)
    return list_of_clubs