# event_routes.py
from fastapi import APIRouter, Query
from app.services import events_service
from typing import List

router = APIRouter()

@router.get("/")
async def get_events(state: str = Query(...), city: str = Query(...)):
    """
    Fetch golf events based on state and city.
    """
    events = events_service.get_events(state, city)
    return events
