from fastapi import APIRouter, Query
from app.services import golf_tips_service
import logging
from random import choice

router = APIRouter()

@router.get("/random")
async def get_random_golf_tip():
    try:
        logging.info("Fetching random golf tip")
        tips = golf_tips_service.get_golf_tips()
        return choice(tips)
    except Exception as e:
        logging.error(f"Error fetching random golf tip: {str(e)}")
        return {"tip": "An unexpected error occurred. Please try again later."}