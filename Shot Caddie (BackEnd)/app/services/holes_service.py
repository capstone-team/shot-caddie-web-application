import logging
from app.data import holes_crud
from app.data.holes_models import Holes, MapHoles
from fastapi import HTTPException
from typing import List

def fetch_holes(courseId: int) -> List[Holes]: 
    logging.info(f"Fetching holes for course: {courseId}")
    holes = holes_crud.get_holes(courseId)
    return holes

def fetch_hole_bounds(courseId: int):
    try:
        return holes_crud.get_hole_bounds(courseId)
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))

