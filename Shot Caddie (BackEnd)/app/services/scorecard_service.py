from app.data import scorecard_crud
from app.data import user_crud
from app.data.scorecard_models import Holes
from app.data.user_schemas import Scorecard
from typing import List
import logging

def fetch_scorecard(courseId: str) -> Scorecard:
    return scorecard_crud.get_scorecard(courseId)

def create_scorecard(scorecard: Scorecard) -> Scorecard:
    logging.info(f"Creating scorecard for course: {scorecard.courseId}")
    return scorecard_crud.create_scorecard(scorecard)

def fetch_scorecards() -> List[Scorecard]:
    logging.info("Fetching scorecards")
    scorecards = scorecard_crud.fetch_scorecards()
    logging.info(f"Fetched {len(scorecards)} scorecards")
    return scorecards








