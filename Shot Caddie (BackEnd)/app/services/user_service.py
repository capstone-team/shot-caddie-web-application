from app.data import user_crud, user_schemas
from app.data.user_schemas import UserScorecard
from app.data.user_models import UserRoundData
from typing import List
from fastapi import HTTPException
import logging

def create_user(user_data: user_schemas.UserCreate):
    try:
        user = user_crud.create_user(user_data.email, user_data.password, user_data.handicap)
        return user
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))
    
def create_user_from_google(user_data: user_schemas.UserReadGoogle):
    try:
        logging.info(f"Creating user in Firestore for email: {user_data.email}")
        return user_crud.create_user_from_google(user_data)
    except Exception as e:
        logging.error(f"Error creating user from Google: {str(e)}")
        raise HTTPException(status_code=400, detail=str(e))


def get_user_by_email(email: str):
    try:
        user, user_data = user_crud.get_user_by_email(email)
        return user, user_data
    except Exception as e:
        raise HTTPException(status_code=404, detail=str(e))

def update_handicap(email: str, handicap: float):
    try:
        user_id, user_email, user_handicap = user_crud.update_user_handicap(email, handicap)
        return user_id, user_email, user_handicap
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))
    
def get_user_handicap(email: str):
    try:
        user, user_handicap = user_crud.get_user_handicap(email)
        return user, user_handicap
    except Exception as e:
        raise HTTPException(status_code=404, detail=str(e))

def add_user_scorecard(email: str, user_scorecard_data: UserScorecard) -> UserScorecard:
    try:
        scorecard = user_crud.save_user_scorecard(email, user_scorecard_data)
        logging.info(f"Scorecard saved for user: {scorecard.email}")
        # Update the user's handicap after adding the new scorecard
        user_crud.update_user_handicap_index(scorecard.email)
        return scorecard
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))
    
def get_user_round(email: str) -> List[UserRoundData]:
    try:
        round_data = user_crud.get_user_rounds(email)
        user_rounds = [UserRoundData(**round) for round in round_data]
        return user_rounds
    except Exception as e:
        logging.error(f"Error fetching user rounds: {str(e)}")
        raise HTTPException(status_code=404, detail=str(e))
    
def get_user_list_of_clubs(email: str) -> dict:
    try:
        user_club_distances = user_crud.get_user_club_distances(email)
        return user_club_distances
    except Exception as e:
        logging.error(f"Error fetching user club distances: {str(e)}")
        raise HTTPException(status_code=404, detail=str(e))
