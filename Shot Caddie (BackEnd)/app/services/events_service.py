from fastapi import HTTPException
from app.data import events_crud

def get_events(state: str, city: str):
    try:
        events = events_crud.fetch_events(state, city)
        if not events:
            raise HTTPException(status_code=404, detail="No events found")
        return events
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    
