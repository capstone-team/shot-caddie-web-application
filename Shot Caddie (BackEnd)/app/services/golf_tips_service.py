from app.data import golf_tips_crud
from fastapi import HTTPException
import logging


def get_golf_tips(): 
    try:
        return golf_tips_crud.get_golf_tips()
    except Exception as e:
        logging.error(f"Error fetching golf tips: {str(e)}")
        raise HTTPException(status_code=500, detail="An unexpected error occurred")